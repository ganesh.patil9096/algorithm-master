package com.tss.algorithm;

import java.util.ArrayList;
import java.util.List;

public class SimpleOne {

	public List<List<String>> simpl1(List<List<String>> clouse) {
		List<String> positive_atoms = clouse.get(0);
		int i = positive_atoms.size();
		List<String> positive_res = new ArrayList<String>();
		for (int x = 0; x < i; x++) {
			boolean to_add = true;
			for (int y = x + 1; y<i; y++) {
				if (positive_atoms.get(x).equals(positive_atoms.get(y))) {
					to_add = false;
				}
			}
			if (to_add)
				positive_res.add(positive_atoms.get(x));
		}
		List<List<String>> finalList = new ArrayList<List<String>>();
		finalList.add(positive_res);
		finalList.add(clouse.get(1));
		return finalList;
	}

	public void printClose(List<List<String>> clouses) {
		try {
			for (List<String> clouse : clouses) {
				System.out.println(clouse.toString());
			}
			System.out.println("---------------- ------------------- ------------------");
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}

	}
}
