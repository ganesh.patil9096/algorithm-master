package com.tss.algorithm;

import java.util.ArrayList;
import java.util.List;

public class Resolution {

	public List<List<String>> universal(List<List<String>> clause) {
		List<String> pos_atoms = clause.get(0);
		List<String> neg_atoms = clause.get(1);
		List<String> pos_res = new ArrayList<String>();
		List<String> neg_res = new ArrayList<String>();
		for (String x : pos_atoms) {
			if (!neg_atoms.contains(x)) {
				pos_res.add(x);
			}
		}
		for (String y : neg_atoms) {
			if (!pos_atoms.contains(y))
				neg_res.add(y);
		}

		List<List<String>> clause_res = new ArrayList<List<String>>();
		clause_res.add(pos_res);
		clause_res.add(neg_res);
		return clause_res;
	}

	public List<List<String>> resolution(List<List<String>> clause1, List<List<String>> clause2) {
		List<String> pos_res = new ArrayList<>();
		pos_res.addAll(clause1.get(0));
		pos_res.addAll(clause2.get(0));
		System.out.println("FIRST :: "+pos_res);
		List<String> neg_res = new ArrayList<>();
		neg_res.addAll(clause1.get(1));
		neg_res.addAll(clause2.get(1));
		System.out.println("SECOND :: "+neg_res);

		List<List<String>> clause = new ArrayList<>();
		clause.add(pos_res);
		clause.add(neg_res);
		SimpleOne simpleOne = new SimpleOne();
		List<List<String>> simpleOneclause = simpleOne.simpl1(clause);
//		simpleOne.printClose(simpleOneclause);

		SimpleTwo simpleTwo = new SimpleTwo();
		List<List<String>> simpleTwoclause = simpleTwo.simpl2(simpleOneclause);
//		simpleTwo.printClose(simpleTwoclause);

		List<List<String>> finalOutput = universal(simpleTwoclause);
//		simpleTwo.printClose(finalOutput);
		
		return finalOutput;
	}
}
