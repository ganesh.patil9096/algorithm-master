package com.tss;

import java.util.ArrayList;
import java.util.List;

import com.tss.algorithm.Resolution;
import com.tss.algorithm.SimpleOne;
import com.tss.algorithm.SimpleTwo;

public class MainProgram {

	public static void main(String[] args) {
		List<String> sampleOne = new ArrayList<String>();
		sampleOne.add("A");
		sampleOne.add("A");
		List<String> sampleTwo = new ArrayList<String>();
		sampleTwo.add("A");
		sampleTwo.add("B");
		sampleTwo.add("C");
		sampleTwo.add("A");
		List<String> sampleThree = new ArrayList<String>();
		sampleThree.add("A");
		sampleThree.add("A");
		sampleThree.add("A");
		List<String> sampleEmpty = new ArrayList<String>();

		SimpleOne simpleOne = new SimpleOne();
		List<List<String>> clouse1 = new ArrayList<List<String>>();

		clouse1.add(sampleOne);
		clouse1.add(sampleEmpty);

		System.out.println("Simple-1 Method output :: ");
		System.out.println("Input :: " + clouse1);
		System.out.println("Output :: ");
		simpleOne.printClose(simpleOne.simpl1(clouse1));

		List<List<String>> clouse2 = new ArrayList<List<String>>();

		clouse2.add(sampleTwo);
		clouse2.add(sampleEmpty);

		System.out.println("Simple-1 Method output :: ");
		System.out.println("Input :: " + clouse2);
		System.out.println("Output :: ");
		simpleOne.printClose(simpleOne.simpl1(clouse2));

		List<List<String>> clouse3 = new ArrayList<List<String>>();

		clouse3.add(sampleEmpty);
		clouse3.add(sampleEmpty);

		System.out.println("Simple-1 Method output :: ");
		System.out.println("Input :: " + clouse3);
		System.out.println("Output :: ");
		simpleOne.printClose(simpleOne.simpl1(clouse3));

		List<List<String>> clouse4 = new ArrayList<List<String>>();

		clouse4.add(sampleThree);
		clouse4.add(sampleEmpty);

		System.out.println("Simple-1 Method output :: ");
		System.out.println("Input :: " + clouse4);
		System.out.println("Output :: ");
		simpleOne.printClose(simpleOne.simpl1(clouse4));

		SimpleTwo simpleTwo = new SimpleTwo();
		List<List<String>> simpleTwoclouse1 = new ArrayList<List<String>>();

		simpleTwoclouse1.add(sampleEmpty);
		simpleTwoclouse1.add(sampleOne);

		System.out.println("Simple-2 Method output :: ");
		System.out.println("Input :: " + simpleTwoclouse1);
		System.out.println("Output :: ");
		simpleTwo.printClose(simpleTwo.simpl2(simpleTwoclouse1));

		List<List<String>> simpleTwoclouse2 = new ArrayList<List<String>>();

		simpleTwoclouse2.add(sampleEmpty);
		simpleTwoclouse2.add(sampleTwo);

		System.out.println("Simple-2 Method output :: ");
		System.out.println("Input :: " + simpleTwoclouse2);
		System.out.println("Output :: ");
		simpleTwo.printClose(simpleTwo.simpl2(simpleTwoclouse2));

		List<List<String>> simpleTwoclouse3 = new ArrayList<List<String>>();

		simpleTwoclouse3.add(sampleEmpty);
		simpleTwoclouse3.add(sampleEmpty);

		System.out.println("Simple-2 Method output :: ");
		System.out.println("Input :: " + simpleTwoclouse3);
		System.out.println("Output :: ");
		simpleTwo.printClose(simpleTwo.simpl2(simpleTwoclouse3));

		List<List<String>> simpleTwoclouse4 = new ArrayList<List<String>>();

		simpleTwoclouse4.add(sampleEmpty);
		simpleTwoclouse4.add(sampleThree);

		System.out.println("Simple-2 Method output :: ");
		System.out.println("Input :: " + simpleTwoclouse4);
		System.out.println("Output :: ");
		simpleTwo.printClose(simpleTwo.simpl2(simpleTwoclouse4));

		Resolution resolution = new Resolution();
		System.out.println("---------------Resolution-------------");

		List<String> l1 = new ArrayList<>();
		l1.add("A");

		List<String> l2 = new ArrayList<>();
		l2.add("B");

		List<String> l3 = new ArrayList<>();
		l3.add("A");
		l3.add("B");

		List<String> l4 = new ArrayList<>();
		l4.add("C");

		List<String> l5 = new ArrayList<>();
		l5.add("A");
		l5.add("A");
		l5.add("B");

		List<String> l6 = new ArrayList<>();
		l6.add("A");
		l6.add("A");

		List<String> l7 = new ArrayList<>();
		l7.add("B");
		l7.add("B");

		List<List<String>> rClause1 = new ArrayList<>();
		rClause1.add(l1);
		rClause1.add(sampleEmpty);

		List<List<String>> rClause2 = new ArrayList<>();
		rClause2.add(sampleEmpty);
		rClause2.add(l1);

		List<List<String>> rClause3 = new ArrayList<>();
		rClause3.add(sampleEmpty);
		rClause3.add(l2);

		List<List<String>> rClause4 = new ArrayList<>();
		rClause4.add(l3);
		rClause4.add(sampleEmpty);

		List<List<String>> rClause5 = new ArrayList<>();
		rClause5.add(sampleEmpty);
		rClause5.add(l3);

		List<List<String>> rClause6 = new ArrayList<>();
		rClause6.add(l2);
		rClause6.add(l4);

		System.out.println("Input :: " + rClause1 + " " + rClause2);
		System.out.println("Output :: ");
		simpleOne.printClose(resolution.resolution(rClause1, rClause2));
		
		System.out.println("Input :: " + rClause3 + " " + rClause4);
		System.out.println("Output :: ");
		simpleOne.printClose(resolution.resolution(rClause3, rClause4));
		
		System.out.println("Input :: " + rClause5 + " " + rClause6);
		System.out.println("Output :: ");
		simpleOne.printClose(resolution.resolution(rClause5, rClause6));

		
	}

}
